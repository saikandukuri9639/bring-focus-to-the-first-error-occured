//for input type date we need to add slds-has-error implicitly
handleValidation() {

    const allInputValid = [
                    ...this.template.querySelectorAll('lightning-input'),
                    ...this.template.querySelectorAll("lightning-combobox"),
                    ...this.template.querySelectorAll('lightning-radio-group')
                ]
                .reduce((validSoFar, inputElement) => {
                    if(inputElement.type === 'date') {
                        if(!inputElement.checkValidity()) {
                            inputElement.classList.add("slds-has-error");
                        }
                        else {
                            inputElement.classList.remove("slds-has-error");
                        }
                    }
                    inputElement.reportValidity();
                    return validSoFar && inputElement.checkValidity();
                }, true);

            var allInput = this.template.querySelector(".slds-has-error");
			if(allInput){
				allInput.scrollIntoView();
				allInput.focus();
			}
}